var express = require("express");
const http = require("http");
var app = express();
const server = http.createServer(app);

const socketIo = require("socket.io")(server, {
  pingTimeout: 60000,
  cors: {
    origin: "*",
  },
});

const onlineUsers = new Set();

function sendNotificationToUser(userId, notification) {
  socketIo.to(userId).emit("notification", notification);
}

socketIo.on("connection", (socket) => {
  console.log("New client connected" + socket.id);

  socket.on("setup", (userData) => {
    socket.join(userData);
    socket.emit("connected");
  });

  socket.on("userOnline", (userId) => {
    onlineUsers.add(userId);
    socket.emit("userStatus", { userId, isOnline: true });
  });

  socket.on("join chat", (room) => {
    socket.join(room);
    console.log("User Joined Room: " + room);
  });

  socket.on("typing", (room) => socket.in(room).emit("typing", room));
  socket.on("stop typing", (room) => socket.in(room).emit("stop typing", room));

  socket.on("new message", (newMessageReceived) => {
    var chat = newMessageReceived.conversationId;

    if (!chat.users) return console.log("chat.users not defined");

    chat.users.forEach((user) => {
      if (user._id !== newMessageReceived.senderId._id) {
        socket.in(user._id).emit("message received", newMessageReceived);
      }
    });
  });

  socket.on("new friend request", (friendRequest) => {
    sendNotificationToUser(friendRequest.receiverId, friendRequest);
  });

  socket.on("new like", (likeData) => {
    sendNotificationToUser(likeData.receiverId, likeData);
  });

  socket.on("new comment", (commentData) => {
    sendNotificationToUser(commentData.receiverId, commentData);
  });

  socket.off("disconnect", () => {
    console.log("User disconnected!");
    onlineUsers.delete(socket.userId);
    socketIo.emit("userStatus", { userId: socket.userId, isOnline: false });
  });
});

server.listen(8900, () => {
  console.log("Server running on port 8900");
});
